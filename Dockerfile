#
# A Dockerfile to build the Health Enterprise Analytics reverse proxy.
#
# Build the image as follows, from this directory:
#     docker build [--no-cache] -t registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-reverse-proxy:<version> -t registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-reverse-proxy:latest .
#
# Optionally, add the --no-cache option if Docker does not seem to pick up your changes to the image.
#
# Login to gitlab via docker then provide the access token as the password it requests
#   docker login registry.gitlab.com -u <gitlab username>
#
# Upload to gitlab.com as follows:
#     docker push registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-reverse-proxy:<version>
#     docker push registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-reverse-proxy:latest
#
# You must be logged into your docker container repository to push.

FROM httpd:2.4.62 as intermediate

RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y build-essential curl dos2unix libcurl4 libcurl4-openssl-dev libapr1 libapr1-dev libaprutil1 libaprutil1-dev libcjose0 libcjose-dev libjansson4 libjansson-dev libpcre3 libpcre3-dev libssl3 libssl-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN curl -LO https://github.com/OpenIDC/mod_auth_openidc/releases/download/v2.4.16.8/mod_auth_openidc-2.4.16.8.tar.gz
RUN tar xvzf mod_auth_openidc-2.4.16.8.tar.gz
RUN cd mod_auth_openidc-2.4.16.8 && ./configure --with-apxs=/usr/local/apache2/bin/apxs && make && make install
ADD ssl-cert-gen.sh ssl-cert-gen.sh 
RUN dos2unix ./ssl-cert-gen.sh

FROM httpd:2.4.62
ENV OIDC_LOG_LEVEL debug
RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y libcjose0 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=intermediate /usr/local/apache2/modules/mod_auth_openidc.so /usr/local/apache2/modules/mod_auth_openidc.so 
COPY --from=intermediate /usr/local/apache2/ssl-cert-gen.sh ./ssl-cert-gen.sh
ADD httpd.conf /usr/local/apache2/conf/httpd.conf
RUN sh ./ssl-cert-gen.sh
