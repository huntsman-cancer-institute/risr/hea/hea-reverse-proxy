cat > sslcert.config <<EOF
[req]
days                           = 365
serial                         = 1
prompt                         = no
default_bits                   = 2048
default_md                     = sha256
distinguished_name             = subject

[subject]
countryName                    = US
stateOrProvinceName            = UT
localityName                   = Salt Lake City
organizationName               = Health Enterprise Analytics 
commonName                     = localhost
emailAddress                   = root@$localhost
EOF

openssl req \
        -x509 \
        -sha256 \
        -nodes \
        -days 365 \
        -config sslcert.config \
        -newkey rsa:2048 \
        -keyout /etc/ssl/private/hea_self_signed.key \
        -out /etc/ssl/certs/hea_self_signed.crt
