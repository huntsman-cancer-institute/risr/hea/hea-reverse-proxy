# HEA Reverse Proxy

A Docker image of a reverse proxy implemented using Apache with the proxy_http plugin. It is designed to be used in 
other projects. It requires a valid user for access to the following paths: /api and /ws (API and Websocket,
respectively).

## Version 1.3.0
* Removing /api/internal location as it isn't used.
* Changes content-type for all text/* to text/plain for more broad generic file preview support.
* Updated mod_auth_openidc.

## Version 1.2.0
* Updated the HTTPD version.
* Support passing the Bearer token to APIs with the access_token query parameter.

## Version 1.1.0
* Blacklist URLs with "internal" in them.

## Version 1
* Initial release.

## Runtime requirements
* Any machine that can run Docker.

## Configuration

Requirements include
- Mounting a file to `/etc/apache2/proxypass.conf` containing ProxyPass and ProxyPassReverse statements (see example below).
- Setting the following environment variables:
  - HEA_REVERSE_PROXY_SERVER_ADMIN=<the webmaster's email address>
  - HEA_REVERSE_PROXY_HOST_NAME=<the public hostname of the server>
  - HEA_REVERSE_PROXY_SCHEME=<whether to connect to the reverse proxy over http or https>
  - OIDC_CRYPTO_PASSPHRASE=<secret> (used internally for OAuth 2)

Other configuration options:
- OIDC_PROVIDER_METADATA_URL=<Open ID Connect provider metadata url (will default to https://host.docker.internal:8444/auth/realms/hea/.well-known/openid-configuration if omitted)
- OIDC_CLIENT_ID=<Open ID Connect client ID (will default to test-api if omitted)
- OIDC_SCOPE=<Open ID Connect scope> (will default to "openid email" if omitted)
- OIDC_REDIRECT_URI=<Open ID Connect redirect URI> (will default to https://localhost:8443/api/redirect if omitted)
- OIDC_REMOTE_USER_CLAIM=<Open ID Connect remote user claim> (will default to email if omitted)
- OIDC_DEFAULT_LOGGED_OUT_URL=<Open ID Connect default logged out URL> (will default to https://host.docker.internal:8444/ if omitted)
- OIDC_OAUTH_VERIFY_JWKS_URI=<Open ID Connect OAuth verify JWKS URI> (will default to https://host.docker.internal:8444/auth/realms/hea/protocol/openid-connect/certs if omitted)
- OIDC_OAUTH_SSL_VALIDATE_SERVER=<Whether or not to validate SSL certificates> (will default to Off -- turn On in production!)
- OIDC_LOG_LEVEL=<Log level for API access requests> (see https://httpd.apache.org/docs/2.4/mod/core.html#loglevel for allowed values; the default is debug)
- TZ=<time zone (will default to America/Denver)

### Example docker-compose.yml
```
services:
    reverseproxy:
        image: registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea/reverseproxy
        ports:
             - ${HEA_REVERSE_PROXY_PORT:-8080}:8080
             - ${HEA_REVERSE_PROXY_SSL_PORT:-8443}:8443
        restart: always
        environment:
            OIDC_CRYPTO_PASSPHRASE: ${OIDC_CRYPTO_PASSPHRASE}
            OIDC_PROVIDER_METADATA_URL: ${OIDC_PROVIDER_METADATA_URL:-https://host.docker.internal:8444/auth/realms/saml-pilot/.well-known/openid-configuration}
            OIDC_CLIENT_ID: ${OIDC_CLIENT_ID:-test-api}
            OIDC_SCOPE: ${OIDC_SCOPE:-openid email}
            OIDC_REDIRECT_URI: ${OIDC_REDIRECT_URI:-https://localhost:8443/api/redirect}
            OIDC_REMOTE_USER_CLAIM: ${OIDC_REMOTE_USER_CLAIM:-email}
            OIDC_DEFAULT_LOGGED_OUT_URL: ${OIDC_DEFAULT_LOGGED_OUT_URL:-https://host.docker.internal:8444/}
            OIDC_OAUTH_VERIFY_JWKS_URI: ${OIDC_OAUTH_VERIFY_JWKS_URI:-https://host.docker.internal:8444/auth/realms/saml-pilot/protocol/openid-connect/certs}
            OIDC_OAUTH_SSL_VALIDATE_SERVER: ${OIDC_OAUTH_SSL_VALIDATE_SERVER:-Off}
            OICD_LOG_LEVEL: ${OIDC_LOG_LEVEL:-debug}
            HEA_REVERSE_PROXY_SERVER_ADMIN: ${HEA_REVERSE_PROXY_SERVER_ADMIN:-webmaster@localhost}
            HEA_REVERSE_PROXY_HOST_NAME: ${HEA_REVERSE_PROXY_HOST_NAME:-localhost}
            HEA_REVERSE_PROXY_SCHEME: ${HEA_REVERSE_PROXY_SCHEME:-https}
            TZ: ${TZ:-America/Denver}
        volumes:
            - type: bind
              source: ./reverseproxy/proxypass.conf
              target: /etc/apache2/proxypass.conf
        extra_hosts:
            - "host.docker.internal:host-gateway"
```

### Example proxypass.conf
```
ProxyPass /api/folders http://heaserver-folders:8086/folders
ProxyPassReverse /api/folders http://heaserver-folders:8086/folders
ProxyPass / http://heawebclient:3000/
ProxyPassReverse / http://heawebclient:3000/
```